#include "lib/merge_lines.h"

/* Merge lines from fd=0 and fd=3 into fd=1 */
int
main(void)
{
	merge_lines(0, 3, 1);
	return 0;
}
