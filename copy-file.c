#include <fcntl.h>
#include <unistd.h>

#include "lib/copy_file.h"

#define write2(x) write(2, x"", sizeof(x) - 1)

int main(int argc, char **argv)
{
	int in, out;
	int err;
	if (argc != 3) {
		write2("usage: copy {source} {destination}\n");
		return 1;
	}
	
	in = open(argv[1], O_RDONLY);
	if (in == -1) {
		write2("error: failed to open source file\n");
		return 1;
	}

	out = open(argv[2], O_WRONLY|O_CREAT, 0644);
	if (out == -1) {
		write2("error: failed to write destination file\n");
		return 1;
	}

	err = copy_file(in, out);
	if (err == -1) {
		write2("error: copy failed");
		return 1;
	}

	return 0;
}
