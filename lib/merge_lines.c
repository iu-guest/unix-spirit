#include <string.h>
#include <unistd.h>

#include "read_line.h"
#include "bwrite.h"
#include "merge_lines.h"

static void
put_read_line(char *str, struct read_line_it *it, struct bwrite *out)
{
	str[it->length] = '\n';
	bwrite_put(out, str, it->length + 1);
}

int
merge_lines(int in1, int in2, int out)
{
	struct read_line_it it1, it2;
	struct bwrite bout;
	char *s1, *s2;

	bwrite_init(&bout, out);

	read_line_init(&it1, in1);
	read_line_init(&it2, in2);

	s1 = read_line_get(&it1);
	s2 = read_line_get(&it2);
	while (s1 && s2) {
		char *next;
		if (strcmp(s1, s2) <= 0) {
			put_read_line(s1, &it1, &bout);
			s1 = read_line_get(&it1);
		} else {
			put_read_line(s2, &it2, &bout);
			s2 = read_line_get(&it2);
		}
	}
	if (!s2) {
		do {
			put_read_line(s1, &it1, &bout);
			s1 = read_line_get(&it1);
		} while (s1);
	} else {
		do {
			put_read_line(s2, &it2, &bout);
			s2 = read_line_get(&it2);
		} while (s2);
	}
	return 0;
}
