#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>

#define write2(x) write(2, x "", sizeof(x) - 1)

static int
separator_index(int argc, char **argv)
{
	int ix;
	for (ix = 1; ix != argc; ++ix)
		if (strcmp(argv[ix], "--") == 0)
			return ix;
	return -1;
}

void
process_spec(char *spec, int *number)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	int port;
	char key[12];
	char value[12];
	struct sockaddr_in sin;

	port = atoi(spec);
	printf("%d\n", port);

	if (sock == -1) {
		write2("xbind: failed to create socket\n");
		exit(1);
	}
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = INADDR_ANY;

	if (bind(sock, (struct sockaddr *)&sin, sizeof(sin)) == -1) {
		write2("xbind: failed to bind socket\n");
		exit(1);
	}
	sprintf(key, "#%d", *number);
	sprintf(value, "%d", sock);
	setenv(key, value, 1);
	*number += 1;
}

/* Run program with specified TCP ports opened for listening.
 *
 * File descriptors are passed to child via environment variables.
 */

int
main(int argc, char **argv)
{
	int sep_ix = separator_index(argc, argv);
	int start;
	char *error;
	int i;
	if (sep_ix == -1 || sep_ix + 1 == argc) {
		write2("usage: xbind [spec ...] -- {prog} [arg ...]\n");
		return 1;
	}

	start = 0;
	for (i = 1; i != sep_ix; ++i) {
		process_spec(argv[1], &start);
	}
	execvp(argv[sep_ix + 1], argv + sep_ix + 1);
	error = strerror(errno);
	write(2, error, strlen(error));
	return 1;
}
