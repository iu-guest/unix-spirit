// Copyright (C) 2017 Dmitry Bogatov <KAction@gnu.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define HELP_MESSAGE                                                \
  "Usage: freezedb out.cdb input.gdbm [another.gdbm ...]        \n" \
  "                                                             \n" \
  "Convert given input GNU DBM database(s) into single          \n" \
  "output constant database. Input databases are processed      \n" \
  "in order. No check for duplicate keys is performed, entries  \n" \
  "after first are lost.\n"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <gdbm.h>
#include <cdb.h>
#include <stdlib.h>
#include <stdio.h>

static void
xcdb_make_start(struct cdb_make *cdbmp, const char *filename)
{
	int fd;
	int err;

	fd = open(filename, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	if (fd < 0) {
		perror(filename);
		exit(41);
	}

	err = cdb_make_start(cdbmp, fd);
	if (err != 0) {
		fprintf(stderr, "failed to create cdb in %s\n", filename);
		exit(1);
	}
}

static void
xcdb_make_finish(struct cdb_make *cdbmp)
{
	int err;

	err = cdb_make_finish(cdbmp);
	if (err != 0) {
		fputs("failed to finalize cdb\n", stderr);
		exit(1);
	}

	err = close(cdbmp->cdb_fd);
	if (err != 0) {
		perror("cdb finalize");
		exit(1);
	}
}

static void
xcopydb(struct cdb_make *cdbmp, GDBM_FILE gdbm)
{
	datum key;
	datum data;

	key = gdbm_firstkey(gdbm);
	while (key.dptr) {
		datum nextkey;
		int err;

		nextkey = gdbm_nextkey(gdbm, key);
		data = gdbm_fetch(gdbm, key);

		err = cdb_make_add(cdbmp, key.dptr, key.dsize,
				   data.dptr, data.dsize);
		if (err != 0) {
			fputs("failed to copy entry", stderr);
			exit(2);
		}

		free(key.dptr);
		key = nextkey;
	}
}

int
main(int argc, char **argv)
{
	struct cdb_make cdbmp;
	int i;

	if (argc < 3) {
		fputs(HELP_MESSAGE, stderr);
		return 41;
	}

	xcdb_make_start(&cdbmp, argv[1]);

	for (i = 2; i != argc; ++i) {
		GDBM_FILE gdbm;

		gdbm = gdbm_open(argv[i], 0, GDBM_READER, 0, NULL);
		if (!gdbm) {
			fputs(gdbm_strerror(gdbm_errno), stderr);
			exit(3);
		}
		xcopydb(&cdbmp, gdbm);
		gdbm_close(gdbm);
	}

	xcdb_make_finish(&cdbmp);
	return 0;
}
